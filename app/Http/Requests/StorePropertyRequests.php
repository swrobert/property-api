<?php

namespace App\Http\Requests;

use App\Property;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StorePropertyRequests extends FormRequest
{

    public function rules()
    {
        return [
            'suburb'   => [
                'required',
            ],
            'state'  => [
                'required',
            ],
            'country' => [
                'required',
            ],

        ];
    }
}
