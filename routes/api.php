<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('property', ['as' => 'store', 'uses' => 'Properties\PropertyController@store']);
Route::post('property/analytics/add', ['as' => 'store', 'uses' => 'Analytics\PropertyAnalyticsController@store']);
Route::post('property/analytics/{id}', ['as' => 'update', 'uses' => 'Analytics\PropertyAnalyticsController@update']);

//Route::get('property', ['as' => 'index', 'uses' => 'Analytics\PropertyAnalyticsController@index']);

//
//Route::get('property/{name}/{value}', [
//    'as' => 'index', 'uses' => 'PropertiesController@index']);

