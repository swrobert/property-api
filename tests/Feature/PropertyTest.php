<?php

namespace Tests\Feature;

use App\Model\Properties\Property;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PropertyTest extends TestCase
{
  //use RefreshDatabase;

    /** @test */
    public function a_suburb_is_required()
    {
        $response = $this->post('/api/property', array_merge($this->data(),['suburb' => '']));
        $response->assertSessionHasErrors(['suburb']);
        $this->assertCount(
            0,
            Property::all(), "Property doesn't contains zero records"
        );
    }

    /** @test */
    public function a_state_is_required()
    {
        $response = $this->post('/api/property', array_merge($this->data(),['state' => '']));
        $response->assertSessionHasErrors(['state']);
        $this->assertCount(
            0,
            Property::all(), "Property doesn't contains zero records"
        );
    }

    /** @test */
    public function a_country_is_required()
    {
        $response = $this->post('/api/property', array_merge($this->data(),['country' => '']));
        $response->assertSessionHasErrors(['country']);
        $this->assertCount(
            0,
            Property::all(), "Property doesn't contains zero records"
        );
    }

    /** @test */
    public function a_property_can_be_added_through_the_api()
    {
        $this->withoutExceptionHandling();

        $this->post('/api/property', $this->data());

        $this->assertCount(1, Property::all());

    }



    /**
     * @return string[]
     */
    private function data()
    {
       return [
            'suburb' => 'Parramatta',
            'state' => 'NSW',
            'country' => 'Australia'
        ];
    }
}
