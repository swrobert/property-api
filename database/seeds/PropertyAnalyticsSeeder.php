<?php

use Flynsarmy\CsvSeeder\CsvSeeder;

class PropertyAnalyticsSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'property_analytics';
        $this->csv_delimiter = ',';

        $this->filename = base_path().'/database/seeds/csvs/property_analytics.csv';

        $this->mapping = [
            0 => 'property_id',
            1 => 'analytic_types_id',
            2 => 'value'
        ];

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        Schema::enableForeignKeyConstraints();


        parent::run();
    }
}
