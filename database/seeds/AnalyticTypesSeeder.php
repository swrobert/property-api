<?php

use Flynsarmy\CsvSeeder\CsvSeeder;

class AnalyticTypesSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'analytic_types';
        $this->csv_delimiter = ',';

        $this->filename = base_path().'/database/seeds/csvs/analytic_types.csv';

        $this->mapping = [
            0 => 'name',
            1 => 'units',
            2 => 'is_numeric',
            3 => 'num_decimal_places'

        ];

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        Schema::enableForeignKeyConstraints();


        parent::run();
    }
}
